###  Please follow these instructions when you try to run this project
#### 1. When you do npm install, you will be asked to select angular version.
#### 2. Please select the latest version shown

```
Application is deployed here.
http://ec2-18-221-241-215.us-east-2.compute.amazonaws.com/

When you try to run this app. 
You must setup the database.
Please find the Sql script in Spa_v3.sql file

npm install
bower install
* Note: please select the latest version of angular when it pop-up

```
