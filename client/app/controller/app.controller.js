(function () {
    angular
        .module("SpaApp")
        .controller("BookingController", BookingController)
        .controller("EditBookingCtrl", EditBookingCtrl)
        .controller("AddBookingCtrl", AddBookingCtrl)
        .controller("DeleteBookingCtrl", DeleteBookingCtrl);
    
    BookingController.$inject = ['$uibModal','UserAPI','SpaAppAPI', '$document', '$rootScope', '$scope', '$state'];
    EditBookingCtrl.$inject = ['SpaAppAPI', '$rootScope', '$scope', '$state','$stateParams'];
    AddBookingCtrl.$inject = ['UserAPI','SpaAppAPI', '$rootScope', '$scope','$state'];
    DeleteBookingCtrl.$inject = ['$uibModalInstance', 'SpaAppAPI', 'items', '$rootScope', '$scope'];
    
    function DeleteBookingCtrl($uibModalInstance, SpaAppAPI, items, $rootScope, $scope){
        var self = this;

        self.deleteBooking = deleteBooking;

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        console.log("items in delete booking crtl");
        console.log(items);
        // console.log(items);
        SpaAppAPI.getBooking(items).then((result)=>{
            console.log(result.data);
            self.booking =  result.data;
        });

        function deleteBooking(){
            console.log("delete booking ...");
            SpaAppAPI.deleteBooking(self.booking.id).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshBookingList');
                $uibModalInstance.close(self.run);
            }).catch((error)=>{
                console.log(error);
            });
        }

    }

    function AddBookingCtrl(UserAPI,SpaAppAPI, $rootScope, $scope, $state){
        console.log("Add Booking");
        var self = this;
        self.timeslotclash = false;
        
        self.istimeslotclash = function(){
            return self.timeslotclash
        }

        $scope.clearslotcheck = function(){
            self.timeslotclash = false
        }

        self.saveBooking = saveBooking;

        self.booking = {}

        self.outlets = [];
        self.services = [];
        self.staffs = [];

        //self.booking.timeslot = self.booking.dob


        SpaAppAPI.getOutlets()
            .then((result) => {
                console.log(result.data)
                var _data = result.data;
                self.outlets = _data.map(function(x) {
                   return {name:x.name, value: x.id};
                });
                console.log(self.outlets)

            }).catch((error)=>{
                console.log(error);
                self.errorMessage = error;
            })
        
        SpaAppAPI.getStaffs()
            .then((result) => {
                console.log(result.data)
                var _data = result.data;
                self.staffs = _data.map(function(x) {
                   return { name:x.name, value: x.id };
                });
                console.log(self.staffs)
            }).catch((error)=>{
                console.log(error);
                self.errorMessage = error;
            })
        
        SpaAppAPI.getServices()
            .then((result) => {
                console.log(result.data)
                var _data = result.data;
                self.services = _data.map(function(x) {
                   return { name:" "+x.type+" "+x.name+" "+x.duration+" Min", value: x.id };
                });
                console.log(self.services)
            }).catch((error)=>{
                console.log(error);
                self.errorMessage = error;
            })

        // self.outlets = [
        //     { name: "Balestier" , value:1},
        //     { name: "Alexandra", value: 2},
        // ];

        // self.staffs = [
        //     { name: "athena" , value:1},
        //     { name: "belinda", value: 2},
        //     { name: "evelyn", value: 3},
        //     { name: "angela", value: 4},
        // ];

        // self.services = [
        //     { name: "bodyMassage-Shiatsu-60Min" , value:1},
        //     { name: "bodyMassage-Shiatsu-90Min", value: 2},
        // ];

        function saveBooking(){
            console.log("save booking ...");
            // console.log(self.booking.name);
            // console.log(self.booking.brand);

            // THIS IS HARD CODED

            // self.booking.user_id = 1
            var user = "davis"
            // SpaAppAPI.getCustomerByName(user).then((result) => {
            UserAPI.getLocalProfile().then(function(result){
                console.log("????????") 
                console.log(result.data.id)

                //var user = "davis"
                var user_id = result.data.id
                self.booking.user_id = user_id

                var time_slot = new Date(self.booking.dob.getFullYear(),
                self.booking.dob.getMonth(),
                self.booking.dob.getDate(),
                self.booking.timeslot.getHours(),
                self.booking.timeslot.getMinutes(),
                self.booking.timeslot.getSeconds(),
                0)
                console.log("time_slot")
                console.log(time_slot)
                self.booking.timeslot = time_slot
                
                SpaAppAPI.addBooking(self.booking).then((result)=>{
                    //console.log(result);
                    console.log("Add booking -> " + result.id);
                    $rootScope.$broadcast('refreshBookingListFromAdd', result.data);
                    $state.go('currentBookings')
                    // $state.go('home') This to redirect once saved successfully
                 }).catch((error)=>{
                    console.log(error);
                    console.log("error 500....>>>>")
                    self.timeslotclash = true
                    self.errorMessage = error;
                 })
                
                // $uibModalInstance.close(self.run);


            }).catch((error)=>{
                console.log(error);
                self.errorMessage = error;
            })

        }
    }
        

    
    function EditBookingCtrl( SpaAppAPI, $rootScope, $scope, $state, $stateParams){
        console.log("Edit Booking Ctrl");
        var self = this;
        self.id = $stateParams.id;
        id = $stateParams.id;

        SpaAppAPI.getBooking(id).then((result)=>{
           console.log(result.data);
           self.booking =  result.data;
        })

        self.saveBooking = saveBooking;

        function saveBooking(){
            console.log("save booking ...");
            console.log(self.booking.name);
            console.log(self.booking.brand);
            SpaAppAPI.updateBooking(self.booking).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshBookingList');
             }).catch((error)=>{
                console.log(error);
             })
            // $uibModalInstance.close(self.run);
            $state.go('home')
        }

    }

    function BookingController($uibModal,UserAPI,SpaAppAPI, $document, $rootScope, $scope,$state) {
        var self = this;
        self.format = "M/d/yy h:mm:ss a";
        self.bookings = [];
        self.maxsize=5;
        self.totalItems = 0;
        self.itemsPerPage = 10;
        self.currentPage = 1;

        self.searchBookings =  searchBookings;
        self.addBooking =  addBooking;
        self.editBooking = editBooking;
        self.deleteBooking = deleteBooking;
        self.pageChanged = pageChanged;

        function searchAllBookings(searchKeyword,orderby,itemsPerPage,currentPage){
            console.log("user.id")
            var user = "davis"
             console.log(user)

            //  UserAPI.getLocalProfile().then(function(result){
            //     console.log(result.data.id);
            //     console.log("DATA....")
            // });


            UserAPI.getLocalProfile().then(function(result){
                console.log("????????") 
                console.log(result.data.id)

                //var user = "davis"
                var user_id = result.data.id
            
                SpaAppAPI.searchBookings(searchKeyword, orderby, itemsPerPage, currentPage,user_id).then((results)=>{
                    
                    self.bookings  = results.data.rows.map(function(bk) {
                        bk.start_time = new Date(bk.start_time)

                        var timeZoneFromDB = +0.00; //time zone value from database
                        //get the timezone offset from local time in minutes
                        var tzDifference = timeZoneFromDB * 60 + bk.start_time.getTimezoneOffset();
                        //convert the offset to milliseconds, add to targetTime, and make a new Date
                        var offsetTime = new Date(bk.start_time.getTime() + tzDifference * 60 * 1000);
                        bk.start_time = offsetTime

                        return bk;
                    });

                    // self.bookings = results.data.rows;
                    

                    console.log(results.data)
                    self.totalItems = results.data.count;
                    $scope.numPages = Math.ceil(self.totalItems /self.itemsPerPage);
                }).catch((error)=>{
                    console.log(error);
                });

             })

        }
        self.searchBookings()
        console.log("USER INFO")
        // console.log(user)


        function pageChanged(){
            console.log("Page changed " + self.currentPage);
            searchAllBookings(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
            console.log($scope.numPages);
        }

        $scope.$on("refreshBookingList",function(){
            console.log("refresh booking list "+ self.searchKeyword);
            searchAllBookings(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
        });

        $scope.$on("refreshBookingListFromAdd",function(event, args){
            console.log("refresh booking list from id"+ args.id);
            var bookings = [];
            bookings.push(args);
            self.searchKeyword = "";
            self.bookings = bookings;
        });

        function searchBookings(){
            console.log("search bookings  ....");
            console.log(self.orderby);
            searchAllBookings(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
        }

        function addBooking(size, parentSelector){
            console.log("post add booking  ....");
            var items = [];
            var parentElem = parentSelector ? 
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './app/view/addBooking.html',
                controller: 'AddBookingCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return items;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }

        function editBooking(id, size, parentSelector){
            $state.go('edit', {'id': id})
        }

        function deleteBooking(id, size, parentSelector){
            console.log("delete Booking...");
            console.log("id > " + id);
            console.log("delete Booking...>>>>>>");
            
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './app/view/deleteBooking.html',
                controller: 'DeleteBookingCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return id;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }
    }
})();