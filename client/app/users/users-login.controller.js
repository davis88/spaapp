/**
 * Created by phangty on 10/11/16.
 */
(function () {
    angular
        .module("SpaApp")
        .controller("LoginCtrl", ["$state", "AuthFactory", "Flash", 'ngProgressFactory', LoginCtrl]);

    function LoginCtrl($state, AuthFactory, Flash, ngProgressFactory){
        var vm = this;

        vm.progressbar = ngProgressFactory.createInstance();

        vm.inputType = 'password';
        vm.passwordCheckbox = false;

        // Hide & show password function
        vm.hideShowPassword = function(){
            if (vm.inputType == 'password')
                vm.inputType = 'text';
            else
                vm.inputType = 'password';
        };

        vm.login = function () {
            vm.progressbar.start();
            AuthFactory.login(vm.user)
                .then(function () {
                    if(AuthFactory.isLoggedIn()){
                        vm.emailAddress = "";
                        vm.password = "";
                        vm.progressbar.complete();
                        $state.go("spaApp");
                    }else{
                        Flash.create('danger', "Ooops having issue logging in!", 0, {class: 'custom-class', id: 'custom-id'}, true);
                        vm.progressbar.stop();
                        $state.go("SignIn");
                    }
                }).catch(function () {
                    vm.progressbar.stop();
                    console.error("Error logging on !");
                });
        };
    }
})();