(function(){

    angular
        .module("SpaApp")
        .controller("ChangeNewPasswordCtrl", ["$state", "UserAPI", ChangeNewPasswordCtrl]);

    function ChangeNewPasswordCtrl($state, UserAPI){
        var vm = this;

        console.log(">>> " + $state.params.token);
        
        vm.resetToken = $state.params.token;
        vm.changePassword = changePassword;
        
        UserAPI.getLocalProfileToken(vm.resetToken)
            .then(function (result) {
                if (result.data != null) { 
                    console.log(result.data);
                    vm.emailAddress = result.data.email;
                } else {
                    console.log("NOT FOUND !!!! Should redirect!");
                    $state.go('SignUp');
                }
            });
            
        function changePassword(){
            console.log("Change Password ...");
            
            UserAPI.changePasswordToken(
                {
                    resetToken:vm.resetToken,
                    NewPassword: vm.NewPassword,
                    ConfirmPassword: vm.ConfirmPassword
                }
            )
            .then(function(result) {
                console.log("SUCCESS!");
                $state.go('SignIn');
            })
            .catch(function(err) {
                console.log("ERROR!");
            });
        }
        
    }
})();
