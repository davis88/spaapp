(function () {
    angular
        .module("SpaApp", [
            // "ngFileUpload",
            "ui.router",
            "ngFlash",
            "ngSanitize",
            "ngProgress",
            "ngMessages",
            "data-table",
            'ui.bootstrap',
            'ui.calendar'
        ]); 

    
})();
// 'ui.bootstrap','ui.router','ui.calendar'.    'ngRoute'


(function(){
    angular
    .module("SpaApp").filter('formatTime', function(){
        return function (time) {
            var date = new Date(time);
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'pm' : 'am';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            return strTime;
          };
        });
})();