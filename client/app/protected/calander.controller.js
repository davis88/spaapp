(function(){

    angular
        .module("SpaApp")
        .controller("CalanderCtrl", ["SpaAppAPI","UserAPI",'$scope', CalanderCtrl]);
    
    // CalanderCtrl.$inject = ['$scope','UserAPI'];

    function CalanderCtrl(SpaAppAPI,UserAPI,$scope,uiCalendarConfig){
        var vm = this;

        $scope.events = [];
        
        $scope.eventSources = [$scope.events]
        

        vm.sociallogins = [];
        UserAPI.getLocalProfile().then(function(result){
            console.log(result.data);
            vm.localProfile = result.data;
        });

        UserAPI.getAllSocialLoginsProfile().then(function(result){
            vm.sociallogins = result.data;
        });


        UserAPI.getLocalProfile().then(function(result) {
      
            var user_id = result.data.id
        
            SpaAppAPI.searchAllBookings(user_id).then((results)=>{
                var _data = results.data;
                console.log(results.data)
      
                for (const index in _data) {
                  var event = _data[index]
                  var start_time = new Date(event.start_time)
                  
                  var timeZoneFromDB = +0.00; //time zone value from database
                  //get the timezone offset from local time in minutes
                  var tzDifference = timeZoneFromDB * 60 + start_time.getTimezoneOffset();
                  //convert the offset to milliseconds, add to targetTime, and make a new Date
                  var offsetTime = new Date(start_time.getTime() + tzDifference * 60 * 1000);
                  start_time = offsetTime
                  
                  $scope.events.push({title: event.service.name + " " + event.service.type, start: start_time,stick: true})

                    

                }
      
            }).catch((error)=>{
                console.log(error);
            });
      
         })

        // for (const index in _data) {
        //     var event = _data[index]
        //     var start_time = new Date(event.start_time)
        //     $scope.events.push({title: event.service.name + " " + event.service.type, start: start_time,stick: true})
        // }

        /* alert on eventClick */
        $scope.alertOnEventClick = function( date, jsEvent, view){
        console.log("CLICKED>..")
      };
  
  
      $scope.eventRender = function( event, element, view ) { 
        console.log("WHEN IT IS CALLED>..")
      };
  
      $scope.changeView = function(view,calendar) {
        console.log(calendar)
        // $scope.events = vm.data
        // $scope.eventSources = [$scope.events]
        // console.log(vm.data)
        // calendar.fullCalendar('changeView',view);
        // calendar.fullCalendar('addEventSource',$scope.eventSources)
      };
  
      $scope.uiConfig = {
        calendar:{
          height: '50%',
          editable: true,
          header:{
            left: 'month agendaWeek agendaDay',
            center: 'title',
            right: 'prev,next'
          },
          eventClick: $scope.alertEventOnClick,
          eventDrop: $scope.alertOnDrop,
          eventResize: $scope.alertOnResize,
          eventRender: $scope.eventRender,
          // viewRender: $scope.changeView
        }
      };

    }
})();
