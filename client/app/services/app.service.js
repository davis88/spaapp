(function(){
    angular
        .module("SpaApp")
        .service("SpaAppAPI", [
            '$http',
            SpaAppAPI
        ]);
    
    function SpaAppAPI($http){
        var self = this;

        self.searchAllBookings = function(user_id){
            // var user_id = 1
            console.log("CUSTOMER ID")
            console.log(user_id)
            return $http.get(`/api/bookings/current/all/${user_id}`);
        }

        // query string
        self.searchBookings = function(value, sortby, itemsPerPage, currentPage,user_id){
            // var user_id = 1
            console.log("CUSTOMER ID")
            console.log(user_id)
            return $http.get(`/api/bookings/current/${user_id}?keyword=${value}&sortby=${sortby}&itemsPerPage=${itemsPerPage}&currentPage=${currentPage}`);
        }

        self.getBooking = function(id){
            console.log(id);
            return $http.get("/api/bookings/" + id)
        }

        self.updateBooking = function(booking){
            console.log(booking);
            return $http.put("/api/bookings",booking);
        }

        self.deleteBooking = function(id){
            console.log(id);
            return $http.delete("/api/bookings/"+ id);
        }
        
        // parameterized values
        /*
        self.searchBookings = function(value){
            return $http.get("/api/bookings/" + value);
        }*/

        // post by body over request
        self.addBooking = function(booking){
            return $http.post("/api/bookings", booking);
        }


        self.getOutlets = function(){
            return $http.get("/api/outlets/")
        }

        self.getStaffs = function(){
            return $http.get("/api/staffs/")
        }

        self.getServices = function(){
            return $http.get("/api/services/")
        }

        self.getCustomerByName = function(user){
            return $http.get("/api/customers/byname/"+user)
        }

    }
})();