var express = require("express");
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require("express-session");
var passport = require("passport");
var flash = require('connect-flash');

var config = require("./config");
var db =  require('./db.js')

var app = express();

app.use(flash());
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// Initialize session
app.use(session({
    secret: "spa-app-secret",
    resave: false,
    saveUninitialized: true
}));

//Initialize passport
app.use(passport.initialize());
app.use(passport.session());

db.then(schema => {
    
        var Models = schema.models
        console.log(Models)
    
        // require('./auth')(app, passport, Models);
        // require('./routes')(app, passport, Models);
    
        require('./auth')(app, passport,Models);
        require('./routes')(app, passport,Models);

        app.listen(config.port, function () {
            console.log("Server running at http://localhost:" + config.port);
        });
})


// app.listen(config.port, function () {
//     console.log("Server running at http://localhost:" + config.port);
// });
