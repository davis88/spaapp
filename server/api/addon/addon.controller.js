module.exports = function (Models) {
    var Addon = Models.Addon;

    get = function (req, res) {
        Addon
            .findById(req.params.id)
            .then(function (addon) {

                if (!addon) {
                    handler404(res);
                }

                res.json(addon);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    list = function (req, res) {
        Addon
            .findAll()
            .then(function (users) {
                res.json(users);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    create = function (req, res) {
        Addon
            .create(req.body)
            .then(function (addon) {
                res.json(addon);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    update = function (req, res) {
        Addon
            .findById(req.params.id)
            .then(function (addon) {

                if (!addon) {
                    handler404(res);
                }

                addon.update(req.body).then(function (addon) {
                    res.json(addon);
                })
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    remove = function (req, res) {
        Addon
            .findById(req.params.id)
            .then(function (addon) {
                if (!addon) {
                    handler404(res);
                }
                addon.destroy({ force: true })
                res.json(addon);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    function handleErr(res) {
        handleErr(res, null);
    }


    function handleErr(res, err) {
        console.log(err);
        res
            .status(500)
            .json({
                error: true
            });
    }

    function handler404(res) {
        res
            .status(404)
            .json({ message: "Addon not found!" });
    }

    function returnResults(results, res) {
        res.send(results);
    }

    return {
        list : list,
        create: create,
        get : get,        
        update: update,
        remove: remove,
    }

}