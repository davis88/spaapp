module.exports = function (Models) {
    var Service = Models.Service;

    get = function (req, res) {
        Service
            .findById(req.params.id)
            .then(function (service) {

                if (!service) {
                    handler404(res);
                }

                res.json(service);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    list = function (req, res) {
        Service
            .findAll()
            .then(function (users) {
                res.json(users);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    create = function (req, res) {
        Service
            .create(req.body)
            .then(function (service) {
                res.json(service);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    update = function (req, res) {
        Service
            .findById(req.params.id)
            .then(function (service) {

                if (!service) {
                    handler404(res);
                }

                service.update(req.body).then(function (service) {
                    res.json(service);
                })
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    remove = function (req, res) {
        Service
            .findById(req.params.id)
            .then(function (service) {
                if (!service) {
                    handler404(res);
                }

                service.destroy({ force: true })
                res.json(service);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    function handleErr(res) {
        handleErr(res, null);
    }


    function handleErr(res, err) {
        console.log(err);
        res
            .status(500)
            .json({
                error: true
            });
    }

    function handler404(res) {
        res
            .status(404)
            .json({ message: "Service not found!" });
    }

    function returnResults(results, res) {
        res.send(results);
    }

    return {
        list : list,
        create: create,
        get : get,        
        update: update,
        remove: remove,
    }

}