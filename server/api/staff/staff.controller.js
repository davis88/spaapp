module.exports = function (Models) {
    var Staff = Models.Staff;

    get = function (req, res) {
        Staff
            .findById(req.params.id)
            .then(function (staff) {

                if (!staff) {
                    handler404(res);
                }

                res.json(staff);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    list = function (req, res) {
        Staff
            .findAll()
            .then(function (users) {
                res.json(users);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    create = function (req, res) {
        Staff
            .create(req.body)
            .then(function (staff) {
                res.json(staff);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    update = function (req, res) {
        Staff
            .findById(req.params.id)
            .then(function (staff) {

                if (!staff) {
                    handler404(res);
                }

                staff.update(req.body).then(function (staff) {
                    res.json(staff);
                })
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    remove = function (req, res) {
        Staff
            .findById(req.params.id)
            .then(function (staff) {
                if (!staff) {
                    handler404(res);
                }

                staff.destroy({ force: true })
                res.json(staff)
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    function handleErr(res) {
        handleErr(res, null);
    }


    function handleErr(res, err) {
        console.log(err);
        res
            .status(500)
            .json({
                error: true
            });
    }

    function handler404(res) {
        res
            .status(404)
            .json({ message: "Staff not found!" });
    }

    function returnResults(results, res) {
        res.send(results);
    }

    return {
        list : list,
        create: create,
        get : get,        
        update: update,
        remove: remove,
    }

}