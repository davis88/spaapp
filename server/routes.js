'use strict';

var UserController = require("./api/user/user.controller");


var express = require("express");
var config = require("./config");

const API_USERS_URI = "/api/users";

const API_STAFFS_URI = "/api/staffs";
const API_SERVICES_URI = "/api/services";
const API_CUSTOMERS_URI = "/api/customers";
const API_OUTLETS_URI = "/api/outlets";
const API_BOOKINGS_URI = "/api/bookings";
const API_ADDONS_URI = "/api/addons";


const HOME_PAGE = "/#!/spaApp";
const SIGNIN_PAGE = "/#!/signIn";


module.exports = function(app, passport,Models) {

    var StaffController = require("./api/staff/staff.controller")(Models);
    var ServiceController = require("./api/service/service.controller")(Models);
    var CustomerController = require("./api/customer/customer.controller")(Models);
    var OutletController = require("./api/outlet/outlet.controller")(Models);
    var BookingController = require("./api/booking/booking.controller")(Models);
    var AddonController = require("./api/addon/addon.controller")(Models);



    // Users API
    app.get(API_USERS_URI, isAuthenticated, UserController.list);
    app.post(API_USERS_URI, isAuthenticated, UserController.create);
    app.get(API_USERS_URI + '/:id', isAuthenticated, UserController.get);
    
    app.post(API_USERS_URI + '/:id', isAuthenticated, UserController.update);
    app.delete(API_USERS_URI + '/:id', isAuthenticated, UserController.remove);
    app.get("/api/user/view-profile", isAuthenticated, UserController.profile);
    app.get("/api/user/social/profiles", isAuthenticated, UserController.profiles);


    // Staffs API    
    app.get(API_STAFFS_URI,isAuthenticated, StaffController.list);    
    app.post(API_STAFFS_URI,isAuthenticated, StaffController.create);
    app.get(API_STAFFS_URI + '/:id',isAuthenticated, StaffController.get);
    app.post(API_STAFFS_URI + '/:id',isAuthenticated, StaffController.update);
    app.delete(API_STAFFS_URI + '/:id',isAuthenticated, StaffController.remove);


    // Services API
    app.get(API_SERVICES_URI,isAuthenticated, ServiceController.list);
    app.post(API_SERVICES_URI,isAuthenticated, ServiceController.create);
    app.get(API_SERVICES_URI + '/:id',isAuthenticated, ServiceController.get);
    app.post(API_SERVICES_URI + '/:id',isAuthenticated, ServiceController.update);
    app.delete(API_SERVICES_URI + '/:id',isAuthenticated, ServiceController.remove);


    // Customers API
    app.get(API_CUSTOMERS_URI,isAuthenticated, CustomerController.list);
    app.post(API_CUSTOMERS_URI,isAuthenticated, CustomerController.create);
    app.get(API_CUSTOMERS_URI + '/:id',isAuthenticated, CustomerController.get);
    app.get(API_CUSTOMERS_URI + '/byname/:firstname',isAuthenticated, CustomerController.getByName);
    
    app.post(API_CUSTOMERS_URI + '/:id',isAuthenticated, CustomerController.update);
    app.delete(API_CUSTOMERS_URI + '/:id',isAuthenticated, CustomerController.remove);


    // Outlets API
    app.get(API_OUTLETS_URI,isAuthenticated, OutletController.list);
    app.post(API_OUTLETS_URI,isAuthenticated, OutletController.create);
    app.get(API_OUTLETS_URI + '/:id',isAuthenticated, OutletController.get);
    app.post(API_OUTLETS_URI + '/:id',isAuthenticated, OutletController.update);
    app.delete(API_OUTLETS_URI + '/:id',isAuthenticated, OutletController.remove);


    // Bookings API
    app.get(API_BOOKINGS_URI,isAuthenticated, BookingController.list);
    app.get(API_BOOKINGS_URI+'/current/:user_id',isAuthenticated, BookingController.getCurrentBookings);
    app.get(API_BOOKINGS_URI+'/current/all/:user_id',isAuthenticated, BookingController.getAllCurrentBookings);
    
    app.post(API_BOOKINGS_URI,isAuthenticated, BookingController.create);
    app.get(API_BOOKINGS_URI + '/:id',isAuthenticated, BookingController.get);
    app.post(API_BOOKINGS_URI + '/:id',isAuthenticated, BookingController.update);
    app.delete(API_BOOKINGS_URI + '/:id',isAuthenticated, BookingController.remove);

    // Addons API
    app.get(API_ADDONS_URI,isAuthenticated, AddonController.list);
    app.post(API_ADDONS_URI,isAuthenticated, AddonController.create);
    app.get(API_ADDONS_URI + '/:id',isAuthenticated, AddonController.get);
    app.post(API_ADDONS_URI + '/:id',isAuthenticated, AddonController.update);
    app.delete(API_ADDONS_URI + '/:id',isAuthenticated, AddonController.remove);


    

    app.get("/protected/", isAuthenticated, function(req, res) {
        if (req.user == null) {
            res.redirect(SIGNIN_PAGE);
        }
    })

    

    app.use(express.static(__dirname + "/../client/"));

    app.post("/change-password", isAuthenticated, UserController.changePasswd);

    app.get("/api/user/get-profile-token", UserController.profileToken);
    app.post("/api/user/change-passwordToken", UserController.changePasswdToken);

    app.post('/register', UserController.register);

    app.post("/login", passport.authenticate("local", {
        successRedirect: HOME_PAGE,
        failureRedirect: "/",
        failureFlash : true
    }));

    app.post("/reset-password", UserController.resetPasswd);

    app.get('/home', isAuthenticated, function(req, res) {
        res.redirect('..' + HOME_PAGE);
    });


    app.get("/oauth/google", passport.authenticate("google", {
        scope: ["email", "profile"]
    }));

    app.get("/oauth/google/callback", passport.authenticate("google", {
        successRedirect: HOME_PAGE,
        failureRedirect: SIGNIN_PAGE
    }));

    app.get("/oauth/facebook", passport.authenticate("facebook", {
        scope: ["email", "public_profile"]
    }));

    app.get("/oauth/facebook/callback", passport.authenticate("facebook", {
        successRedirect: HOME_PAGE,
        failureRedirect: SIGNIN_PAGE,
        failureFlash: true
    }));

    app.get('/oauth/linkedin',
        passport.authenticate('linkedin', { state: 'SOME STATE' }),
        function(req, res) {
            // The request will be redirected to LinkedIn for authentication, so this
            // function will not be called.
        });

    app.get('/oauth/linkedin/callback', passport.authenticate('linkedin', {
        successRedirect: HOME_PAGE,
        failureRedirect: SIGNIN_PAGE,
        failureFlash: true
    }));

    app.get('/oauth/wechat', passport.authenticate('wechat'));

    app.get('/oauth/wechat/callback', passport.authenticate('wechat', {
        failureRedirect: SIGNIN_PAGE,
        successReturnToOrRedirect: HOME_PAGE,
        failureFlash: true
    }));

    app.get('/oauth/twitter', passport.authenticate('twitter'));

    // handle the callback after twitter has authenticated the user
    app.get('/oauth/twitter/callback',
        passport.authenticate('twitter', {
            successRedirect: HOME_PAGE,
            failureRedirect: SIGNIN_PAGE
        }));

    app.get("/status/user", function(req, res) {
        var status = "";
        if (req.user) {
            status = req.user.email;
        }
        console.info("status of the user --> " + status);
        res.send(status).end();
    });

    app.get("/logout", function(req, res) {
        req.logout(); // clears the passport session
        req.session.destroy(); // destroys all session related data
        res.send(req.user).end();
    });


    function isAuthenticated(req, res, next) {
        if (req.isAuthenticated())
            return next();
        res.redirect(SIGNIN_PAGE);
    }
    app.use(function(req, res, next) {
        if (req.user == null) {
            res.redirect(SIGNIN_PAGE);
        }
        next();
    });

};