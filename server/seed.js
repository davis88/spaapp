var bcrypt = require('bcryptjs');
var config = require("./config");
var database = require("./database");
var User = database.User;
var Post = database.Post;

module.exports = function() {
    if (config.seed) {
        var hashpassword = bcrypt.hashSync("Password@123", bcrypt.genSaltSync(8), null);
        User
            .create({
                username: "admin@spa.com",
                password: hashpassword,
                firstName: "Admin",
                lastName: "Admin",
                addressLine1: "79 02 Ayer Rajah crescent",
                addressLine2: "",
                city: "Singapore",
                email: "admin@spa.com",
                postcode: "42312",
                isAdmin: true,
                phone: "90909090",
                google: "http://google.com",
                facebook: "http://facebook.com",
                twitter: "http://twitter.com"
            })
            .then(function(user) {
                console.log(user);
            }).catch(function() {
                console.log("Error", arguments)
            })

        User
            .create({
                username: "davis@gmail.com",
                password: hashpassword,
                firstName: "Davis",
                lastName: "Lim",
                addressLine1: "79 02 Ayer Rajah Crescent",
                addressLine2: "",
                city: "Singapore",
                email: "davis@gmail.com",
                postcode: "42312",
                phone: "90909090",
                google: "http://google.com",
                facebook: "http://facebook.com",
                twitter: "http://twitter.com"
            })
            .then(function(user) {
                console.log(user)
            })
            .catch(function() {
                console.log("Error", arguments)
            })

    }
};